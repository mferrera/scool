#include <pybind11/pybind11.h>
#include <functional>

#include "scool/simple_executor.hpp"
#include "scool/partitioner.hpp"
#include "universal_task.hpp"
#include "universal_state.hpp"

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

using simple_partitioner = scool::simple_partitioner<Py_Universal_Task>;
using simple_executor = scool::simple_executor<Py_Universal_Task, Universal_State>;
using simple_context = scool::simple_context<simple_executor, true>;
using iterator = std::vector<Py_Universal_Task>::iterator;

PYBIND11_MODULE(SCoOL, m) {
    m.doc() = "SCoOL Module";

    py::class_<simple_partitioner>(m, "simple_partitioner")
        .def(py::init<>());

    py::class_<Universal_Task, Py_Universal_Task>(m, "Universal_Task")
        .def(py::init<>())
        .def("process", &Universal_Task::py_process)
        .def("setBuffer", &Universal_Task::setBuffer)
        .def("getBuffer", &Universal_Task::getBuffer);

    py::class_<Universal_State>(m, "Universal_State")
        .def(py::init<>())
        .def("identity", &Universal_State::identity);

    py::class_<simple_executor>(m, "simple_executor")
        .def(py::init<>())
        /* .def("init", py::overload_cast<iterator, iterator, const Universal_State &, const simple_partitioner &>(&simple_executor::init), */
        /*         py::arg("first"), */
        /*         py::arg("last"), */
        /*         py::arg("st"), */
        /*         py::arg("pt") = simple_partitioner()) */
        /* .def("init", py::overload_cast<const Py_Universal_Task &, const Universal_State &, const simple_partitioner &>(&simple_executor::init), */
        /*         py::arg("t"), */
        /*         py::arg("st"), */
        /*         py::arg("pt") = simple_partitioner()) */
        .def("init", &simple_executor::init_,
                py::arg("t"), py::arg("st"), py::arg("pt") = simple_partitioner())
        .def("step", &simple_executor::step);

    py::class_<simple_context>(m, "simple_context")
        .def(py::init<simple_executor&>(), py::return_value_policy::reference)
        .def("push", &simple_context::push);

#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
