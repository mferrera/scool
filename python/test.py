from SCoOL import *
import pickle

tasks = []

class task(Universal_Task):
    def __init__(self):
        Universal_Task.__init__(self)

    def process(self, ctx, st):
        buf = self.getBuffer()
        props = pickle.loads(buf)

        if (props.curr < props.n):
            print("Props : {}, Level : {}".format(props.p, props.curr))
            props.curr += 1
            for i in range(props.curr, props.n):
                t = task()
                tasks.append(t)

                props.p[i], props.p[props.curr] = props.p[props.curr], props.p[i]
                buf_props = pickle.dumps(props)

                t.setBuffer(buf_props)
                ctx.push(t)
                props.p[i], props.p[props.curr] = props.p[props.curr], props.p[i]
        else:
            print("Complete :", props.p)


class state(Universal_State):
    pass


class task_props:
    curr = -1
    n = 5
    p = [i for i in range(n)]

    def __getstate__(self):
        attributes = {}
        for k in [a for a in dir(self) if not a.startswith('__')]:
            attributes[k] = self.__getattribute__(k)
        return attributes


p = task_props()
p_a = pickle.dumps(p)
t = task()
t.setBuffer(p_a)
s = state()
a = simple_executor()
a.init(t, s)
while(a.step() > 0):
    pass
