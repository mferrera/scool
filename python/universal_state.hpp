#ifndef universal_state_hpp
#define universal_state_hpp

class Universal_State {
public:
   Universal_State() {}

   void identity() {}

   void operator+=(const Universal_State& st);

   void operator==(const Universal_State& st) const;
}; // class Universal_State

std::ostream& operator<<(std::ostream&os, const Universal_State& st);

std::istream& operator>>(std::istream& is, Universal_State& st);

#endif // universal_state_hpp
