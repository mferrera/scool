#ifndef universal_task_hpp
#define universal_task_hpp

#include <pybind11/pybind11.h>

#include "universal_state.hpp"
#include "scool/simple_executor.hpp"

class Py_Universal_Task;
using simple_executor = scool::simple_executor<Py_Universal_Task, Universal_State>;
using simple_context = scool::simple_context<simple_executor, true>;

class Universal_Task {
public:
    Universal_Task() {}

    virtual void py_process(simple_context& ctx, Universal_State& st) = 0;

    void merge(const Universal_Task& t);

    void setBuffer(const std::string& buffer) { buffer_ = buffer; }

    pybind11::bytes getBuffer() { return buffer_; }
    pybind11::bytes buffer_;
}; // class Universal_Task

class Py_Universal_Task : public Universal_Task {
public:
    /* Inherit the constructors */
    using Universal_Task::Universal_Task;

    std::function<void(simple_context &, Universal_State &)> func = 0;

    Py_Universal_Task() {
        func = std::bind(&Py_Universal_Task::py_process,
                    this, 
                    std::placeholders::_1,
                    std::placeholders::_2
                );
    }

    void py_process(simple_context& ctx, Universal_State& st) override {
        PYBIND11_OVERRIDE_PURE_NAME(
            void,           // Return type
            Universal_Task, // 
            "process",      // Function called from Python
            py_process,     // Name of function in C++ 
            ctx,            // Arguments follow
            st
        );
    }

    void process(simple_context& ctx, Universal_State& st) {
       if (func) func(ctx, st); 
    }
}; // class Py_Universal_task

#endif // universal_task_hpp
